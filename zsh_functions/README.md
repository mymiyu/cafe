## Make a folder for your zsh functions & copy function definitions in it
```
mkdir $HOME/.zfunc
cp -r zsh-functions/_* $HOME/.zfunc
```

## Edit your ~/.zshenv 
```
FPATH=$HOME/.zfunc:$FPATH
autoload -Uz ~/.zfunc/**/*
```
