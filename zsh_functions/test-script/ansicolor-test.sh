#!/usr/bin/env zsh

echo "This is" $(coloredText 'coloredtext' 'normal' 'white') "test"
echo "This is" $(coloredText 'coloredtext' 'bold' 'white') "test"
echo "This is" $(coloredText 'coloredtext' 'bold' 'blue' 'white') "test"
echo "This is" $(coloredText 'coloredtext' 'blink' 'blue') "test"
