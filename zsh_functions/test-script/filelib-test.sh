#!/usr/bin/env zsh

the_file="/path/file.ext"

echo getDirName  : $(getDirName $the_file)
echo getFileName : $(getFileName $the_file)
echo getFileBase : $(getFileBase $the_file)
echo getFileExt  : $(getFileExt $the_file)
echo getFileBase from getFileName : $(getFileBase $(getFileName $the_file))
