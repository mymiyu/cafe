#!/usr/bin/env zsh

if [ "$1" = "" ]
then
    echo "$0 <image to save>"
    exit 0
fi

img=$1
saveas=$img.tar

if [ -f "$saveas" ]
then
    echo $(coloredText 'FAILED:' 'bold' 'red' 'white') file exists
    exit 0
fi

docker save -o $saveas $img

echo $(coloredText 'SAVED:' 'bold' 'red' 'white') $saveas
