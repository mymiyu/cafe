#!/usr/bin/env zsh

regaddr=https://devnas.myds.me:5050
regauth=docker:docker1234

image=$1

if [[ "$image" == "" ]]
then
    echo "$0 <image to delete>"
    exit 0
fi

repo=$(echo $image | cut -f1 -d:)
tag=$(echo $image | cut -f2 -d:)

header="Accept: application/vnd.docker.distribution.manifest.v2+json"
response=$(curl -u $regauth -vsH $header $regaddr/v2/$repo/manifests/$tag 2>&1)
digest=$(echo "$response" | grep -i "< docker-content-digest:" | awk '{print $3}')
digest=${digest//[$'\t\r\n']}
result=$(curl -u $regauth -w "%{http_code}" -sH $header -X DELETE $regaddr/v2/$repo/manifests/$digest)

if [ "$result" = "202" ]
then
    echo $(coloredText 'DELETED:' 'bold' 'red' 'white') $repo:$tag
else
    echo $(coloredText 'FAILED:' 'bold' 'red' 'white') $result
fi

