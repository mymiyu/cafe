#!/usr/bin/env bash

USER=$(whoami)

if [ $# -eq 1 ]
then
    DOCKER_IMG=$1
    RUN_AS=$USER
elif [ $# -eq 2 ]
then
    RUN_AS=$1
    DOCKER_IMG=$2
else
    echo "$0 {user} <docker image>"
    exit 1
fi

DOCKER_MNTS="-v $(pwd):/proj"
DOCKER_OPTS="--gpus all -w /proj"

docker run -it $DOCKER_MNTS $DOCKER_OPTS -u $RUN_AS $DOCKER_IMG
