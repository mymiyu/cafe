#!/usr/bin/env zsh

while IFS= read -r img
do
    docker rmi -f $img
done <<< $(docker images | grep '<none>' | awk '{print $3}')
