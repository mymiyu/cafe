#!/usr/bin/env zsh

regaddr=https://devnas.myds.me:5050
regauth=docker:docker1234

echo ' docker images on registry server:' $(coloredText $regaddr 'bold' 'blue')
echo '------------------------------------------------------------------'

while IFS= read -r repo
do
    while IFS= read -r tag
    do
        if [ "$tag" = "null" ]
        then
            echo " " $(coloredText $repo 'bold' 'yellow'):$(coloredText '(empty)' 'normal' 'red')
        else
            echo " " $(coloredText $repo 'bold' 'yellow'):$(coloredText $tag 'normal' 'green')
        fi
    done <<< $(getRegTags $regaddr $regauth $repo)
done <<< $(getRegRepos $regaddr $regauth)

