#!/usr/bin/env zsh

img=$1

if [ "$img" = "" ]
then
    echo "$0 <image file to load>"
    exit 0
fi

if [ ! -f "$img" ]
then
    echo $(coloredText 'FAILED:' 'bold' 'red' 'white') file not found
    exit 0
fi

docker load -i $img

echo $(coloredText 'LOADED:' 'bold' 'red' 'white') $img
