#!/usr/bin/env zsh

regaddr=devnas.myds.me:5050
reguser=docker
regpass=docker1234
registry=$regaddr

img=$1
imgas=$img

if [ "$img" = "" ]
then
    echo "$0 <server image to pull> <local image name>"
    exit 0
elif [ "$2" != "" ]
then
    imgas=$2
fi

docker login -u $reguser -p $regpass $regaddr
docker pull $registry/$img
docker tag $registry/$img $imgas
docker rmi $registry/$img

echo $img $(coloredText '>> PULL >>' 'bold' 'red' 'white') $imgas
