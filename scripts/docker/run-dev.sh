#!/usr/bin/env bash

CMD=$*

if [ "$CMD" = "" ]
then
    echo "$0 <command>"
    exit 1
fi

RUN_AS=$(whoami)
DOCKER_IMG=devimg:latest
DOCKER_MNTS="-v $(pwd):/proj"
DOCKER_OPTS="--gpus all -w /proj"

docker run -it $DOCKER_MNTS $DOCKER_OPTS -u $RUN_AS $DOCKER_IMG $CMD
