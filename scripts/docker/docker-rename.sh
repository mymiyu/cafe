#!/usr/bin/env zsh

argn=$#

if [[ $argn -ne 2 ]]
then
    echo "$0 <from> <to>"
    exit 0
fi

fromimg=$1
toimg=$2

docker tag $fromimg $toimg
docker rmi $fromimg

echo $fromimg $(coloredText '>> RENAME >>' 'bold' 'red' 'white') $toimg
