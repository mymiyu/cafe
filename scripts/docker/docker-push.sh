#!/usr/bin/env zsh

regaddr=devnas.myds.me:5050
reguser=docker
regpass=docker1234
registry=$regaddr

img=$1
imgas=$img

if [[ "$img" == "" ]]
then
    echo "$0 <local image to push> <server image name>"
    exit 0
elif [[ "$2" != "" ]]
then
    imgas=$2
fi

docker login -u $reguser -p $regpass $regaddr
docker tag $img $registry/$imgas
docker push $registry/$imgas
docker rmi $registry/$imgas

echo $img $(coloredText '>> PUSH >>' 'bold' 'red' 'white') $imgas
