#!/usr/bin/env zsh

avifile="$*"
mp4file="$(getFileBase "${avifile}").mp4"

ffmpeg -i "${avifile}" -max_muxing_queue_size 999999 -vcodec libx264 "${mp4file}"
