#!/usr/bin/env zsh

print INPUT: $0 $*
print TOTAL: $# arguments

let n=1
print -n PARAMS:
for param in $*
do
  print -n " ($n) $param"
  let n++
done

print '\n'
