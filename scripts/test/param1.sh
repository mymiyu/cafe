#!/usr/bin/env zsh

echo "\$0 - " $0
echo "\$# - " $#
echo "\$@ - " $@
echo "\$* - " $*
echo "\$1 - " $1
echo "\$2 - " $2
echo "\$3 - " $3

