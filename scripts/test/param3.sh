#!/usr/bin/env zsh

echo INPUT: $0 $*
echo TOTAL: $# arguments

n=1
echo -n PARAMS:
for param in $*
do
  echo -n "($n) $param "
  n=$(($n+1))
done

echo '\n'
