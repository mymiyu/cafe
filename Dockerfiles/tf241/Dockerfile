#
# based on ubuntu:20.04
#

FROM ubuntu:20.04

ENV PATH /usr/local/cuda/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/bin:/sbin
ENV LANG C.UTF-8
ENV TERM xterm
ENV DEBIAN_FRONTEND noninteractive

#
# RUN sed -i -e 's/http:\/\/archive.ubuntu.com/http:\/\/mirror.kakao.com/g' /etc/apt/sources.list
#

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y --no-install-recommends \
        gnupg2 \
        software-properties-common \
        sudo \
        zsh \
        vim \
        python3.8 \
        python3.8-dev \
        python3-pip \
        python3-setuptools \
        python3-wheel \
        python3-virtualenv \
        libgtk2.0-dev \
    &&  rm -rf /var/lib/apt/lists/*

RUN update-alternatives --install /usr/bin/python python /usr/bin/python3 1

# Add cuda repository

RUN apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/7fa2af80.pub && \
    add-apt-repository "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64 /" && \
    add-apt-repository "deb https://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu2004/x86_64 /" && \
    apt-get update

# CUDA 11.0, NCCL 2.8.3, CUDNN 8.0.5

ENV CUDA_PKG_VERSION 11-0
ENV NCCL_VERSION 2.8.4
ENV CUDNN_VERSION 8.0.5.39

ENV LD_LIBRARY_PATH /usr/local/cuda/lib64:$LD_LIBRARY_PATH
ENV LIBRARY_PATH /usr/local/cuda/lib64/stubs:$LIBRARY_PATH

ENV NVIDIA_VISIBLE_DEVICES all
ENV NVIDIA_DRIVER_CAPABILITIES compute,utility
ENV NVIDIA_REQUIRE_CUDA "cuda>=11.0 brand=tesla,driver>=418,driver<419 brand=tesla,driver>=440,driver<441 brand=tesla,driver>=450,driver<451"

RUN apt-get install -y --no-install-recommends \
        cuda-toolkit-$CUDA_PKG_VERSION \
        libnccl2=$NCCL_VERSION-1+cuda11.0 \
        libnccl-dev=$NCCL_VERSION-1+cuda11.0 \
        libcudnn8=$CUDNN_VERSION-1+cuda11.0 \
        libcudnn8-dev=$CUDNN_VERSION-1+cuda11.0 \
    &&  rm -rf /var/lib/apt/lists/*

RUN echo "/usr/local/cuda/lib64" > /etc/ld.so.conf.d/nvidia.conf

# PyTorch 1.7.1

RUN pip3 install --upgrade pip

RUN pip install tensorflow==2.4.1

# USERS

COPY skel/adduser.conf /etc/adduser.conf
COPY skel/sudoers /etc/sudoers
COPY skel/profile /etc/skel/.profile
COPY skel/bashrc /etc/skel/.bashrc
COPY skel/bash_aliases /etc/skel/.bash_aliases
#COPY skel/zshrc /etc/skel/.zshrc
#COPY skel/zsh_aliases /etc/skel/.zsh_aliases
COPY skel/vimrc /etc/skel/.vimrc

RUN adduser --uid 1000 --gid 100 --disabled-password --gecos '' user
RUN usermod -aG sudo user
RUN echo "user:password" | chpasswd

WORKDIR /proj
